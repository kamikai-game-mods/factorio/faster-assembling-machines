data:extend(
{
  {
    type = "recipe",
    name = "assembling-machine-4",
    enabled = false,
    ingredients =
    {
      {"speed-module-2", 2},
      {"processing-unit", 1},
      {"assembling-machine-3", 1}
    },
    result = "assembling-machine-4"
  },
  {
    type = "recipe",
    name = "assembling-machine-5",
    enabled = false,
    ingredients =
    {
      {"speed-module-3", 2},
      {"processing-unit", 2},
      {"assembling-machine-4", 1}
    },
    result = "assembling-machine-5"
  },
  {
    type = "recipe",
    name = "assembling-machine-6",
    enabled = false,
    ingredients =
    {
      {"speed-module-3", 4},
      {"processing-unit", 5},
      {"assembling-machine-5", 1}
    },
    result = "assembling-machine-6"
  },
}
)
  