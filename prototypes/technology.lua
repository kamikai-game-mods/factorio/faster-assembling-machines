data:extend(
{

  {
    type = "technology",
    name = "automation-4",
    icon = "__base__/graphics/technology/automation.png",
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "assembling-machine-4"
      }
    },
    prerequisites = {"speed-module-2", "automation-3"},
    unit =
    {
      count = 100,
      ingredients = 
      {
        {"alien-science-pack", 2},
        {"science-pack-1", 1}, 
        {"science-pack-2", 1}, 
        {"science-pack-3", 1}
      },
      time = 60
    },
    order = "a-b-c"
  },
  {
    type = "technology",
    name = "automation-5",
    icon = "__base__/graphics/technology/automation.png",
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "assembling-machine-5"
      }
    },
    prerequisites = {"speed-module-3", "automation-4"},
    unit =
    {
      count = 100,
      ingredients = 
      {
        {"alien-science-pack", 4},
        {"science-pack-1", 2}, 
        {"science-pack-2", 2}, 
        {"science-pack-3", 2}
      },
      time = 90
    },
    order = "a-b-c"
  },
  {
    type = "technology",
    name = "automation-6",
    icon = "__base__/graphics/technology/automation.png",
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "assembling-machine-6"
      }
    },
    prerequisites = {"automation-5"},
    unit =
    {
      count = 100,
      ingredients = 
      {
        {"alien-science-pack", 10},
        {"science-pack-1", 5}, 
        {"science-pack-2", 5}, 
        {"science-pack-3", 5}
      },
      time = 180
    },
    order = "a-b-c"
  },
}
)